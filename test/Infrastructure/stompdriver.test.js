const mock = require("mock-require");
const expect = require("expect.js");
const sinon = require("sinon");

const StompClientMethods = {
  connect: () => {},
  subscribe: () => {},
  unsubscribe: () => {},
  debug: () => {}
};

class Publisher{
  constructor() {
    console.log("Mocked Publisher");
  }
  dispatch(){}
  on(){}
  once(){}
  destroy(){}
}

const stompClient = sinon.spy(() => StompClientMethods);

mock("../../site/src/Infrastructure/Publisher", Publisher);
mock("../../site/src/Infrastructure/application.config", {
  STOMP_URL: "test"
});
const { StompDriver } = require("../../site/src/Infrastructure/StompDriver");
describe("StompDriver", () => {
  let ScopedStompDriver;
  before(() => {
    global.DEBUG = false;
    global.Stomp = {
      client: stompClient
    };
    global.Stomp.client.resetHistory();
    ScopedStompDriver = StompDriver;
  });
  it("should create new StompDriver instances with no error", () => {
    const stompDriverInstance = new ScopedStompDriver();
    expect(stompDriverInstance instanceof ScopedStompDriver).to.be(true);
  });
  it("should also be a Publisher", () => {
    const stompDriverInstance = new ScopedStompDriver();
    expect(stompDriverInstance instanceof Publisher).to.be(true);
  });

  describe("constructor", () => {
    it("should call Stomp.client", () => {
      new ScopedStompDriver();
      expect(global.Stomp.client.calledWith("test")).to.be(true);
    });
  });

  describe("connect", () => {
    it("should call Stomp client connect and dispatch a success event on connection success", () => {
      const instance = new ScopedStompDriver();
      const spy = sinon.spy();
      sinon.stub(StompClientMethods, "connect").callsFake((data, success) => success());
      sinon.stub(Publisher.prototype, "dispatch").callsFake(spy);
      instance.connect();
      expect(spy.called).to.be(true);
    });
  });

  describe("subscribe", () => {
    it("should call Stomp client subscribe with a destination and dispatch an event on callback", () => {
      const instance = new ScopedStompDriver();
      const stub = sinon.stub().returns({body:JSON.stringify({message:""})});
      sinon.stub(StompClientMethods, "subscribe").callsFake(
        () => stub()
      );
      instance.subscribe("destination");
      expect(stub.called).to.be(true);
    });
  });

  describe("unsubscribe", () => {
    it("should call Stomp client unsubscribe with a destination", () => {
      const instance = new ScopedStompDriver();
      const stub = sinon.stub();
      sinon.stub(StompClientMethods, "unsubscribe").callsFake(
        () => stub()
      );
      instance.unsubscribe("destination");
      expect(stub.called).to.be(true);
    });
  });
});
