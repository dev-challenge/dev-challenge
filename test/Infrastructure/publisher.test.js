const mock = require("mock-require");
const Publisher = require("../../site/src/Infrastructure/Publisher");
const expect = require("expect.js");

mock.stop("../../site/src/Infrastructure/Publisher");
describe("Publisher", () => {
  it("should create new Publisher instances with no error", () => {
    const publisherInstance = new Publisher();
    expect(publisherInstance instanceof Publisher);
  });

  describe("on", () => {
    it("should register new listeners", () => {
      const publisherInstance = new Publisher();
      const callback = () => console.log("onTest");
      publisherInstance.on("onTest", callback);
      expect(publisherInstance.eventMap.get("onTest").length).to.be(1);
    });
  });

  describe("once", () => {
    it("should register new listeners", () => {
      const publisherInstance = new Publisher();
      const callback = () => console.log("onceTest");
      publisherInstance.on("onceTest", callback);
      expect(publisherInstance.eventMap.get("onceTest").length).to.be(1);
    });
  });

  describe("dispatch", () => {
    it("should run \"on\" listeners every time given action is dispatched", () => {
      const publisherInstance = new Publisher();
      let counter = 0;
      publisherInstance.on("onDispatchTest", () => counter++);
      publisherInstance.dispatch("onDispatchTest");
      publisherInstance.dispatch("onDispatchTest");
      expect(counter).to.be(2);
    });
    it("should run \"once\" listeners only once when given action is dispatched", () => {
      const publisherInstance = new Publisher();
      let counter = 0;
      publisherInstance.once("onceDispatchTest", () => counter++);
      publisherInstance.dispatch("onceispatchTest");
      publisherInstance.dispatch("onceDispatchTest");
      expect(counter).to.be(1);
    });
  });

  describe("destroy", () => {
    it("should remove given event", () => {
      const publisherInstance = new Publisher();
      publisherInstance.on("destroyTest1", () => {});
      publisherInstance.on("destroyTest2", () => {});
      expect(publisherInstance.eventMap.size).to.be(2);
      publisherInstance.destroy("destroyTest1");
      expect(publisherInstance.eventMap.size ).to.be(1);
      expect(publisherInstance.eventMap.has("destroyTest2"));
    });
  });
});

