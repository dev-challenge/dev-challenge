const mock = require("mock-require");
const expect = require("expect.js");
mock("../../site/src/Infrastructure/application.config", {
  SPARKLINE_MAX_AGE_IN_MS: 2
});

class Publisher{
  constructor() {
    console.log("Mocked Publisher");
  }
  dispatch(){}
  on(){}
  once(){}
  destroy(){}
}
mock("../../site/src/Domain/FxDomain", Publisher);

const {
  __test__: {
    filterDataPointByAge,
    getCachedSparkline,
    updateSparkline,
    parseDataToTable,
    sortDataMatrixByLastChangedBidAmount
  }
} = require("../../site/src/Application/fxTable.controller");

class MockedSparkline{}

const mockedFxData = {
  usdjpy: [{
    name: "usdjpy",
    bestBid: 106.7297012204255,
    bestAsk: 107.25199883791178,
    lastChangeAsk: -4.862314256927661,
    lastChangeBid: -2.8769211401569663,
    avg: 1,
  },{
    name: "usdjpy",
    bestBid: 106.7297012204255,
    bestAsk: 107.25199883791178,
    lastChangeAsk: -4.862314256927661,
    lastChangeBid: -2.8769211401569663,
    avg: 2,
  },],
  gbpjpy: [
    {
      name: "gbpjpy",
      bestBid: 106.7297012204255,
      bestAsk: 107.25199883791178,
      lastChangeAsk: -4.862314256927661,
      lastChangeBid: -2.8769211401569663,
      avg: 1,
    },
    {
      name: "gbpjpy",
      bestBid: 106.7297012204255,
      bestAsk: 107.25199883791178,
      lastChangeAsk: -4.862314256927661,
      lastChangeBid: -2.8769211401569663,
      avg: 2,
    },],
  gbpchf: [
    {
      name: "gbpchf",
      bestBid: 106.7297012204255,
      bestAsk: 107.25199883791178,
      lastChangeAsk: -4.862314256927661,
      lastChangeBid: -2.8769211401569663,
      avg: 1,
    },
    {
      name: "gbpchf",
      bestBid: 106.7297012204255,
      bestAsk: 107.25199883791178,
      lastChangeAsk: -4.862314256927661,
      lastChangeBid: -2.8769211401569663,
      avg: 2,
    },],
  gbpaud: [
    {
      name: "gbpaud",
      bestBid: 106.7297012204255,
      bestAsk: 107.25199883791178,
      lastChangeAsk: -4.862314256927661,
      lastChangeBid: -2.8769211401569663,
      avg: 1,
    },
    {
      name: "gbpaud",
      bestBid: 106.7297012204255,
      bestAsk: 107.25199883791178,
      lastChangeAsk: -4.862314256927661,
      lastChangeBid: -2.8769211401569663,
      avg: 2,
    },]
};

const mockedDataMatrix = [
  [null,null,null,null,2],
  [null,null,null,null,3],
  [null,null,null,null,-5],
  [null,null,null,null,4]
];

describe("fxTable.controller", () => {
  before(() => {
    global.document = {
      createElement: () => "element"
    },
    global.Sparkline = MockedSparkline
  });
  describe("filterDataPointByAge", () => {
    it("should return a function which validates if point is greater than a number minus SPARKLINE_MAX_AGE_IN_MS", () => {
      const filterFn = filterDataPointByAge({timestamp: 3});

      expect(filterFn({timestamp:1})).to.be(false);
      expect(filterFn({timestamp:2})).to.be(true);
    });
  });
  describe("getCachedSparkline", () => {
    it("should return the given key of the given object if it exists", () => {
      const obj = {test: 123};
      expect(getCachedSparkline(obj, "test")).to.be(123);
    });
    it("should mutate given object, adding it the correct structure", () => {
      const obj = {};
      const testSubject = getCachedSparkline(obj, "test");
      expect(obj.test).to.be(testSubject);
      expect(testSubject.sparkElement).to.be("element");
      expect(testSubject.chart instanceof MockedSparkline).to.be(true);
      expect(Array.isArray(testSubject.series)).to.be(true);
    });
  });
  describe("updateSparkline", () => {
    it("should add an fx rate to given sparkline", () => {
      const testSparkline = {
        series: []
      };
      updateSparkline(testSparkline, {avg: 1, timestamp: 2});
      expect(testSparkline.series[0].avg).to.be(1);
      expect(testSparkline.series[0].timestamp).to.be(2);
    });
  });
  describe("parseDataToTable", () => {
    it("given a list of fx, should return a bi-dimensional representation of a table", () => {
      const mockedGetCachedSparkline = () => ({
        chart: {draw: () => {}},
        series: []
      });
      const parsedData = parseDataToTable(mockedFxData, mockedGetCachedSparkline);
      expect(parsedData.length).to.be(4);
      expect(parsedData[0].length).to.be(6);
      expect(parsedData[0][0]).to.be(mockedFxData.usdjpy[0].name);
      expect(parsedData[0][1]).to.be(mockedFxData.usdjpy[0].bestBid);
      expect(parsedData[0][2]).to.be(mockedFxData.usdjpy[0].bestAsk);
      expect(parsedData[0][3]).to.be(mockedFxData.usdjpy[0].lastChangeAsk);
      expect(parsedData[0][4]).to.be(mockedFxData.usdjpy[0].lastChangeBid);
      expect(parsedData[0][5]).to.be(mockedFxData.usdjpy[0].undefined);
      expect(parsedData[1].length).to.be(6);
      expect(parsedData[2].length).to.be(6);
      expect(parsedData[3].length).to.be(6);
    });
  });
  describe("sortDataMatrixByLastChangedBidAmount", () => {
    it("should fail", () => {
      const sortedData = sortDataMatrixByLastChangedBidAmount([...mockedDataMatrix]);
      expect(sortedData[0][4]).to.be(-5);
      expect(sortedData[1][4]).to.be(4);
      expect(sortedData[2][4]).to.be(3);
      expect(sortedData[3][4]).to.be(2);
    });
  });
});