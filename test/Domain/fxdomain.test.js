const mock = require("mock-require");
const sinon = require("sinon");
const expect = require("expect.js");


mock("../../site/src/Infrastructure/application.config", {
  FX_DOMAIN_MAX_RATES: 2
});
class FxStompRepo{
  constructor() {
    console.log("Mocked FxStompRepo");
    this.config = {
      events: {
        connection_success: "mocked_connection_success",
      }
    };
  }
  subscribe(){}
  connect(){}
  dispatch(){}
  on(){}
  once(){}
  destroy(){}
}

class Publisher{
  constructor() {
    console.log("Mocked Publisher");
  }
  dispatch(){}
  on(){}
  once(){}
  destroy(){}
}

mock("../../site/src/Domain/FxRepo", {
  FxStompRepo,
  FX_REPO_CONSTANTS: {
    DESTINATION: {
      RATE: "MOCK"
    }
  }
});
mock.stop("../../site/src/Domain/FxDomain");
const FxDomain = require("../../site/src/Domain/FxDomain");

describe("FxDomain", () => {
  before(() => {
    mock("../../site/src/Infrastructure/Publisher", Publisher);
  });
  after(() => {
    mock.stop("../../site/src/Infrastructure/Publisher");
  });
  it("Should connect to repo upon instantiation", () => {
    const spy = sinon.spy();
    sinon.stub(FxStompRepo.prototype, "connect").callsFake(spy);
    new FxDomain();
    expect(spy.called).to.be(true);
  });
  it("Should subscribe destination on repo", () => {
    const onceSpy = sinon.spy();
    const subscribeSpy = sinon.spy();
    sinon.stub(FxStompRepo.prototype, "once").callsFake((event, cb) => {
      onceSpy(event);
      cb();
    });
    sinon.stub(FxStompRepo.prototype, "subscribe").callsFake(subscribeSpy);
    new FxDomain();
    expect(onceSpy.lastCall.firstArg).to.be("mocked_connection_success");
    expect(subscribeSpy.lastCall.firstArg).to.be("MOCK");
  });
  it("Should listen to data events", () => {
    const onSpy = sinon.spy();
    sinon.stub(FxStompRepo.prototype, "on").callsFake((event, cb) => {
      onSpy(event);
    });
    new FxDomain();
    expect(onSpy.lastCall.firstArg).to.be("MOCK");
  });
  it("Should update its fx values ", () => {
    const instance = new FxDomain();
    instance.updateFx('mockFx', {name: 'mockFx'});
    console.log(instance.fxRates.mockFx[0]);
    expect(instance.fxRates.mockFx[0].name).to.be('mockFx');
  });
});